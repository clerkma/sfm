#! /system/bin/sh/texlua

-- 2013/01/02 initial version 
-- 2013/01/02 commandline wrap
kpse.set_program_name(arg[0])

require("lfs")
require("alt_getopt")

function hashdir(des)
local header = 
"% ls-R -- filename database for kpathsea; do not change this line."
local dir = {}

local function storedir(path)
    dir[1] = "./"
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local f = path..'/'..file
            local attr = lfs.attributes (f)
            if attr.mode == "directory" then
                dir[#dir+1] = f
                storedir(f)
            end
        end
    end
end

local function attrdir(path)
    for file in lfs.dir(path) do
        if file ~= "." and file ~= ".." then
            local f = path..'/'..file
            local attr = lfs.attributes (f)
            if string.sub(f, -1) ~= "~" then
                io.write(file.."\n")
            end
        end
    end
end

lfs.chdir(des)
storedir(".")
print("mktexlsr: Updating "..des.."/ls-R...")
io.output("ls-R", "w")
io.write(header.."\n\n")
for i = 1, #dir do
    entry = tostring(dir[i])..":\n"
    io.write(entry)
    attrdir(dir[i])
    io.write("\n")
end
io.close()
print("mktexlsr: Updated "..des.."/ls-R...")
end

function showlsr(sstr)
    local lsRt = {}
    local lsRs = sstr
    local enu
    repeat
        enu = lsRs:find(";")
        if enu then
            lsRt[#lsRt+1] = string.sub(lsRs, 1, enu-1)
            lsRs = string.sub(lsRs, enu + 1, -1)
        end
    until lsRs:find(";") == nil
    lsRt[#lsRt+1] = lsRs
    return lsRt
end

function main()
    local lsRs = kpse.show_path("ls-R")
    local lsRt = showlsr(lsRs)
    for i = 1, #lsRt do
        hashdir(lsRt[i])
    end
end
